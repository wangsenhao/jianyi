package com.example.mrwang.jianyi;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button recommend;
    private Button discover;
    private Button my_bas;
    private String data;
    private String user_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        data = intent.getStringExtra("data");
        user_phone = intent.getStringExtra("user_phone");
        recommend=(Button)findViewById(R.id.recommend);
        discover=(Button)findViewById((R.id.discover));
        my_bas=(Button)findViewById(R.id.my_bas);
        recommend.setOnClickListener(lis0);
        discover.setOnClickListener(lis0);
        my_bas.setOnClickListener(lis0);
    }

    View.OnClickListener lis0=new View.OnClickListener(){
        public void onClick (View v){
            if (v.getId()==R.id.recommend)
            {
                FragmentManager manager =getSupportFragmentManager();
                huiyi huiyiFragment=new huiyi();
                FragmentTransaction transaction =manager.beginTransaction();
                transaction.replace(R.id.fragment_container,huiyiFragment);
                transaction.commit();
            }
            else if(v.getId()==R.id.discover)
            {
                FragmentManager manager =getSupportFragmentManager();
                lianxiren lianxirenFragment=new lianxiren();
                FragmentTransaction transaction =manager.beginTransaction();
                transaction.replace(R.id.fragment_container,lianxirenFragment);
                transaction.commit();
            }
            else if(v.getId()==R.id.my_bas)
            {
                FragmentManager manager =getSupportFragmentManager();
                wode my_activityFragment=new wode();
                FragmentTransaction transaction =manager.beginTransaction();
                transaction.replace(R.id.fragment_container,my_activityFragment);
                transaction.commit();
            }
        }
    };


}
