package com.example.mrwang.jianyi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/******  登陆activity  *****/

public class Main2Activity extends AppCompatActivity {
    private Button btn;   //跳转到注册按钮
    private Button login_btn;  //登陆按钮
    private EditText edit_uphone;
    private EditText edit_upwd;
    private String uphone;
    private String upwd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        edit_uphone=(EditText)findViewById(R.id.edit_phone);
        edit_upwd=(EditText)findViewById(R.id.edit_pwd);
        login_btn=(Button)findViewById(R.id.denglu);
        btn=(Button)findViewById(R.id.zhuce);

        //获得输入的用户名密码
        uphone=edit_uphone.getText().toString();
        upwd=edit_upwd.getText().toString();

        //button设置监听
        login_btn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                //调用登录接口

                //跳转到主界面
                Intent intent =new Intent();
                intent.setClass(Main2Activity.this,MainActivity.class);
                Main2Activity.this.startActivity(intent);
            }
        });

        btn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent =new Intent();
                intent.setClass(Main2Activity.this,Main3Activity.class);
                Main2Activity.this.startActivity(intent);
            }
        });
    }
}
