package entity;

/**
 * Created by Med on 2017/12/2.
 */

public class User {
    private String uid;
    private String uname;
    private String upwd;
    private String email;
    private String phone;
    private int sex;        // 1 - 男 ， 2 - 女

    public User(){
        this.uid="111";     //目构造时默认
        this.phone="00000000000";
        this.email="111";
        this.sex=1;
        this.upwd="123456";
        this.uname="111";
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUpwd() {
        return upwd;
    }

    public void setUpwd(String upwd) {
        this.upwd = upwd;
    }


}
